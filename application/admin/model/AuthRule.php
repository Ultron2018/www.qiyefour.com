<?php
namespace app\admin\model;

use think\Model;

class AuthRule extends Model
{
    public function authRuleTree()
    {
        $authRuleres = $this->order('sort ASC')->select();
        return $this->sort($authRuleres);
    }

    public function sort($data,$pid=0)
    {
        static $arr=array();
        foreach($data as $k=>$v){
            if($v['pid']==$pid){
                $v['dataid']=$this->getparentid($v['id']);
                $arr[]=$v;
                $this->sort($data,$v['id']);
            }
        }

        return $arr;
    }

    public function getchildrenid($authRuleId)
    {
        $AuthRuleRes=$this->select();
        return $this->_getchildrenid($AuthRuleRes,$authRuleId);
    }

    public function _getchildrenid($AuthRuleRes,$authRuleId)
    {
        static $arr = array();
        foreach ($AuthRuleRes as $k=>$v){
            if($v['pid']==$authRuleId){
                $arr[]=$v['id'];
                $this->_getchildrenid($AuthRuleRes,$v['id']);
            }
        }
        return $arr;
    }


    public function getparentid($authRuleId)
    {
        $AuthRulesRes=$this->select();
        return $this->_getparentid($AuthRulesRes,$authRuleId,True);
    }

    public function _getparentid($AuthRulesRes,$authRuleId,$clear=false)
    {
        static $arr=array();
        if($clear){
            $arr = array();
        }
        foreach($AuthRulesRes as $k=>$v){
            if($v['id']==$authRuleId){
                $arr[] = $v['id'];
                $this->_getparentid($AuthRulesRes,$v['pid'],False);
            }
        }
        asort($arr);
        $arrStr=implode('-',$arr);
        return $arrStr;
    }





}
