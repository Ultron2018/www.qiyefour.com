<?php
/**
 * Created by PhpStorm.
 * User: wangkxin@foxmail.com
 * Date: 2020/9/3
 * Time: 11:19
 */

namespace app\admin\validate;
use think\Validate;

class Admin extends Validate
{
    protected $rule=[
        'name'=>'unique:admin|require|max:25',
        'password'=>'require|min:6',
    ];

    protected $message=[
        'name.require'=>'管理员名称不得为空',
        'name.unique'=>'管理员名称不得重复',
        'name.max'=>'管理员名称不得超过25位',
        'password.require'=>'密码不得为空',
        'password.min'=>'管理员密码不得小于6位',


    ];

    protected  $scene=[
        'add'=>['name','password'],
        'edit'=>['name','password'=>'min:6'],
    ];




}