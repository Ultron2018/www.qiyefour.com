<?php
/**
 * Created by PhpStorm.
 * User: wangkxin@foxmail.com
 * Date: 2020/9/3
 * Time: 11:19
 */

namespace app\admin\validate;
use think\Validate;

class Cate extends Validate
{
    protected $rule =[
        'catename'=>'unique:cate|require|max:25',
    ];

    protected $message=[
        'catename.unique'=>'栏目名称不得重复',
        'catename.require'=>'栏目名称不得为空',
        'catename.max'=>'栏目名称最大不得超过25位字符',
    ];

    protected $scene = [
        'add'=>['catename'],
        'edit'=>['catename'],
    ];




}