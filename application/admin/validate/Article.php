<?php
/**
 * Created by PhpStorm.
 * User: wangkxin@foxmail.com
 * Date: 2020/9/3
 * Time: 11:19
 */

namespace app\admin\validate;
use think\Validate;

class Article extends Validate
{
   protected $rule =[
       'title'=>'unique:article|require|max:25',
       'cateid'=>'require',
       'content'=>'require',
   ];

   protected $message = [
       'title.unique'=>'文章标题不得重复',
       'title.require'=>'文章标题不得为空',
       'title.max'=>'文章标题最大长度不得超过25个字符',
       'content.require'=>'文章内容不能空',
   ];

   protected $scene = [
        'add'=>['title','cateid','content'],
        'edit'=>['title','cateid'],
   ];




}