<?php
/**
 * Created by PhpStorm.
 * User: wangkxin@foxmail.com
 * Date: 2020/9/1
 * Time: 15:01
 */

return [
    'template' =>[
        //模板引擎类型 支持PHP think 支持扩展
        'view_suffix'=>'html'
    ],
    'view_replace_str'=>[
        '__PUBLIC__'=>'/public/',
        '__ROOT__'=>'/',
        '__ADMIN__'=>'http://www.qiyefour.com/static/admin'
    ]
];