<?php
namespace app\admin\controller;
use app\admin\model\Article as ArticleModel;
use app\admin\model\Cate as CateModel;
use think\Db;

class Article extends Common
{

    public function lst()
    {
        $artres = db('article')->field('a.*, b.catename')->alias('a')->join('bk_cate b','a.cateid=b.id')->order('a.id desc')->paginate(10);
        $this->assign('artres',$artres);
        return view('list');
    }

    public function add()
    {
        $cateM = new CateModel();
        $cateres = $cateM->catetree();
        $this->assign('cateres',$cateres);

       if(request()->isPost()){
           $data=input('post.');
           $data['time']=time();
           $validate =  \think\Loader::validate('Article');
           if(!$validate->scene('add')->check($data)){
               $this->error($validate->getError());
           }
           $articleM = new ArticleModel();
           // if($_FILES['thumb']['tmp_name']){
           //     $file=request()->file('thumb');
           //     $info = $file->move(ROOT_PATH . 'public\static' . DS .'uploads');
           //     if($info){
           //          $thumb='http://www.qiyefour.com/'. 'static' . DS . 'uploads' . '/' .$info->getSaveName();
           //          $data['thumb'] = $thumb;
           //     }
           // }

           if($articleM->save($data)){
               $this->success('添加文章成功',url('lst'));
           }else{
               $this->error('添加文章失败');
           }
           return;
       }
        return view();
    }


    public function edit($id)
    {
        if(request()->isPost()){
            $data = input('post.');
            $validate = \think\Loader::validate('Article');
            if(!$validate->scene('edit')->check($data)){
                $this->error($validate->getError());
            }
            $artM = new ArticleModel();
            $save =  $artM->update($data);
            if($save !== false){
                $this->success('修改文章成功',url('lst'));
            }else{
                $this->error('修改文章失败');
            }
            return;
        }
        $cateM = new CateModel();
        $cateres = $cateM->catetree();
        $arts = db('article')->where(array('id'=>$id))->find();
        $this->assign(array(
            'cateres'=>$cateres,
            'arts'=>$arts,
        ));
        return view();
    }

    public function del($id)
    {
        $artM=new ArticleModel();
        $delnum = $artM->where('id',$id)->delete();
        if($delnum=='1'){
            $this->success('删除文章成功',url('lst'));
        }else{
            $this->error('删除文章失败');
        }
    }




}
