<?php
namespace app\admin\controller;
use think\Controller;
use app\admin\model\Admin;
class Login extends Controller
{
    public function index()
    {
        if(request()->isPost()){
            $this->check(input('code'));
            $data = input('post.');
            $adminM = new Admin();
            $num = $adminM->login($data);
            if($num=='1'){
                $this->error('管理员不存在');
            }
            if($num=='2'){
                $this->success('登录成功',url('index/index'));
            }
            if($num=='3'){
                $this->error('登录密码错误');
            }

        }
        return view('login');
    }

   // 验证码检测
    public function check($code='')
    {
        $captcha = new \think\captcha\Captcha();
        if(!$captcha->check($code)){
            $this->error('验证码错误');
        }else{
            return true;
        }
    }


}
