<?php
namespace app\admin\controller;

use app\admin\model\Conf as ConfModel;
use think\Db;

class Conf extends Common
{

    public function lst()
    {
        if(request()->isPost()){
            $sorts = input('post.');
            $confM = new ConfModel();
            foreach($sorts as $k=>$v){
                $confM->update(['id'=>$k,'sort'=>$v]);
            }
            $this->success('更新排序成功',url('lst'));
            return;
        }
        $confres = ConfModel::order('sort ASC')->paginate(5);
        $this->assign('confres',$confres);
        return view('list');
    }

    public function add()
    {
        if(request()->isPost()){
            $data = input('post.');
            $validate = \think\Loader::validate('Conf');
            if(!$validate->check($data)){
                $this->error($validate->getError());
            }
            if($data['values']){
                 $data['values'] = str_replace('，', ',',$data['values']);
            }
            $conf =  new ConfModel();
            if($conf->save($data)){
                $this->success('添加配置成功',url('lst'));
            }else{
                $this->error('添加配置失败');
            }
            return;
        }
        return view();
    }


    public function edit($id)
    {
        if(request()->isPost()){
            $data =  input('post.');
            $validate = \think\Loader::validate('Conf');
            if(!$validate->scene('edit')->check($data)){
                $this->error($validate->getError());
            }

            if($data['values']){
                $data['values'] = str_replace('，',',',$data['values']);
            }
            $confM =  new ConfModel();
            $save =  $confM->update($data);
            if($save !== false){
                $this->success('配置项修改成功',url('lst'));
            }else{
                $this->error('配置项修改失败');
            }
            return;
        }
        $confs = ConfModel::find($id);
        $this->assign('confs',$confs);
        return view();
    }

    public function del($id)
    {
        $del = ConfModel::destroy($id);
        if($del){
            $this->success('删除配置项成功',url('lst'));
        }else{
            $this->error('删除配置项失败');
        }
    }


    public function conf()
    {
        if(request()->isPost()){
            $data = input('post.');
            $formarr=array();
            foreach($data as $k=>$v){
                $formarr[]=$k;
            }
            $_confarr=db('conf')->field('enname')->select();
            $confarr=array();
            foreach($_confarr as $k=>$v){
                $confarr[]=$v['enname'];
            }
            $checkboxarr=array();
            foreach($confarr as $k=>$v){
                if(!in_array($v, $formarr)){
                    $checkboxarr[]=$v;
                }
            }

            if($data){
                foreach($data as $k=>$v){
                    ConfModel::where('enname',$k)->update(['value'=>$v]);
                }
                $this->success('修改配置成功');
            }
            return;
        }
        $confres = ConfModel::order('sort ASC')->select();
        $this->assign('confres',$confres);
        return view('conf');
    }




}
