<?php
namespace app\admin\controller;

use app\admin\model\Link as LinkModel;
use think\Db;

class Link extends Common
{

    public function lst()
    {
        $linkM = new LinkModel();
        if(request()->isPost()){
            $sorts = input('post.');
            foreach($sorts as $k=>$v){
                $linkM->update(['id'=>$k,'sorts'=>$v]);
            }
            $this->success('更新排序成功',url('lst'));
            return;
        }
        $linkres =  LinkModel::order('sorts asc')->paginate(5);
        $this->assign('linkres',$linkres);
        return view('list');
    }

    public function add()
    {
        if(request()->isPost()){
            $linkM = new LinkModel();
            $data =  input('post.');
            $validate = \think\Loader::validate('Link');
            if(!$validate->scene('add')->check($data)){
                $this->error($validate->getError());
            }
            $add = $linkM->save($data);
            if($add){
                $this->success('添加友情链接成功',url('lst'));
            }else{
                $this->error('添加友情链接失败');
            }
            return;
        }
        return view();
    }


    public function edit($id)
    {
        if(request()->isPost()){
            $linkM = new LinkModel();
            $data = input('post.');
            $validate =  \think\Loader::validate('Link');
            if(!$validate->scene('edit')->check($data)){
                $this->error($validate->getError());
            }
            // $save=LinkModel::update($data);
            $save = $linkM->save($data,['id'=>$id]);
            if($save !== false){
                $this->success('修改友情链接成功',url('lst'));
            }else{
                $this->error('修改友情链接失败');
            }
            return;
        }
        $links = LinkModel::find($id);
        $this->assign('links', $links);
        return view();
    }

    public function del($id)
    {
        $del = linkModel::destroy($id);
        if($del){
            $this->success('删除链接成功',url('lst'));
        }else{
            $this->error('删除链接失败');
        }

    }




}
