<?php
namespace app\index\controller;
use app\index\model\Article as ArticelModel;
class Article extends Common
{
    public function index()
    {
        $artid = input('artid');
        db('article')->where(array('id'=>$artid))->setInc('click');
        $articles = db('article')->find($artid);
        $artM = new ArticelModel();
        $hots = $artM->getHotRes($articles['cateid']);
        $this->assign(array(
            'articles'=>$articles,
            'hots'=>$hots
        ));
        return view('article');
    }

    //点赞
    public function zan($ip,$artid)
    {
        $data = ['ip'=>$ip,'artid'=>$artid];
        $add = db('zan')->insert($data);
        if($add){
            echo 1;
        }else{
            echo 2;
        }
    }


}
