<?php
namespace app\index\controller;
use think\Controller;
use app\index\model\Conf as ConfModel;
use app\index\model\Cate as CateModel;
class Common extends Controller
{
    public function _initialize()
    {
        //当前位置
        if(input('cateid')){
            $this->getPos(input('cateid'));
        }
        if(input('artid')){
            $articles = db('article')->field('cateid')->find(input('artid'));
            $cateid = $articles['cateid'];
            $this->getPos($cateid);
        }

        //网站配置项
        $this->getConf();
        //网站栏目导航
        $this->getNavCates();
        //底部导航信息
        $cateM = new CateModel();
        $recBottom = $cateM->getRecBottom();
        $this->assign('recBottom',$recBottom);
    }

    #获取导航栏目
    public function getNavCates()
    {
        $cateres = db('cate')->where(array('pid'=>0))->select();
        foreach($cateres as $k=>$v){
            $children = db('cate')->where(array('pid'=>$v['id']))->select();
            if($children){
                $cateres[$k]['children']=$children;
            }else{
                $cateres[$k]['children']=0;
            }
        }
        // dump($cateres);
        $this->assign('cateres',$cateres);
    }

    //网站配置项
    public function getConf()
    {
        $conf =new ConfModel();
        $_confres = $conf->getAllConf();
        $confres = array();
        foreach($_confres as $k=>$v){
            $confres[$v['enname']]= $v['cnname'];
        }
        $this->assign('confres',$confres);
    }


    //当前位置的实现
    public function getPos($cateid)
    {
        $cate = new \app\index\model\Cate();
        $postArr = $cate->getparents($cateid);
        $this->assign('postArr',$postArr);
    }





}
