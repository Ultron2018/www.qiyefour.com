<?php
namespace app\index\controller;
use app\index\model\Cate as CateModel;
class Page extends Common
{
    public function index($cateid)
    {
        $catepage = db('cate')->find($cateid);
        $cateM = new CateModel();
        $cateInfo = $cateM->getCateInfo($cateid);
        $this->assign(array(
            'cateInfo'=>$cateInfo,
            'catepage'=>$catepage
        ));
        return view('page');
    }
}
