<?php
namespace app\index\controller;
use app\index\model\Article;
use app\index\model\Cate as CateModel;
class Imglist extends Common
{
    public function index($cateid)
    {
        $artM = new Article();
        $artRes = $artM->getAllArticles($cateid);
        $cateM = new CateModel();
        $cateInfo = $cateM->getCateInfo($cateid);
        $this->assign(array(
            'artRes'=>$artRes,
            'cateInfo'=>$cateInfo
        ));
        return view('imglist');
    }
}
