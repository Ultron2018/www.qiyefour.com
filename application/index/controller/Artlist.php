<?php
namespace app\index\controller;
use app\index\model\Article;
use app\index\model\Cate as CateModel;
class Artlist extends Common
{
    public function index()
    {
        $cateid = input('cateid');
        $artcleM = new Article();
        $artRes = $artcleM->getAllArticles($cateid);
        $hotRes = $artcleM->getHotRes($cateid);
        $cateM = new CateModel();
        $cateInfo = $cateM->getCateInfo($cateid);
        $this->assign(array(
            'artRes'=>$artRes,
            'hotRes'=>$hotRes,
            'cateInfo'=>$cateInfo
        ));
        return view('artlist');
    }
}
