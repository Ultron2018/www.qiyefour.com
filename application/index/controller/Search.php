<?php
namespace app\index\controller;
use app\index\model\Article as ArticleModel;
class Search extends Common
{
    public function index()
    {
        $keywords = input('keywords');
        $artM = new ArticleModel();
        $searchHot = $artM->searchHot();
        if($keywords){
            $serRes = db('article')
                ->where('title','like','%'.$keywords.'%')
                ->order('id DESC')
                ->paginate(2,false,$config=['query'=>array('keywords'=>$keywords)]);
            $this->assign(array(
                'serRes'=>$serRes,
                'keywords'=>$keywords,
                'searchHot'=>$searchHot
            ));
        }

        return view('search');
    }
}
