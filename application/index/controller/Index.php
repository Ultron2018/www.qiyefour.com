<?php
namespace app\index\controller;
use app\index\model\Article as ArticleModel;
use app\index\model\Cate as CataModel;
class Index extends Common
{
    public function index()
    {
        //首页最新文章调用
        $artM = new ArticleModel();
        $newArts = $artM->getNewArticle();
        $siteHot = $artM->getSiteHot();
        $getRec = $artM->getRecArt();
        //获取推荐栏目
        $cateM = new CataModel();
        $getRecIndex = $cateM->getRecIndex();

        //友情链接数据
        $linkRes = db('link')->order('sorts DESC')->select();
        $this->assign(array(
            'newArts'=>$newArts,
            'siteHot'=>$siteHot,
            'linkRes'=>$linkRes,
            'getRec'=>$getRec,
            'getRecIndex'=>$getRecIndex
        ));
        return view();
    }
}
