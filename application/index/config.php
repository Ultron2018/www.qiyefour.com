<?php
/**
 * Created by PhpStorm.
 * User: wangkxin@foxmail.com
 * Date: 2020/9/1
 * Time: 16:33
 */
return [
    'template' =>[
        //模板引擎类型 支持PHP think 支持扩展
        'view_suffix'=>'html'
    ],
    'view_replace_str'=>[
        '__PUBLIC__'=>'/public/',
        '__ROOT__'=>'/',
        '__INDEX__'=>'http://www.qiyefour.com/static/index',
    ]
];