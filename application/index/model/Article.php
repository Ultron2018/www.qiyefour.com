<?php
namespace app\index\model;
use think\Model;
use \app\index\model\Cate;

class Article extends Model
{
    public function getAllArticles($cateid)
    {
        $cateM = new Cate();
        $allCateid = $cateM->getchildrenid($cateid);
        $artRes = db('article')->where("cateid IN($allCateid)")->paginate(8);
        return $artRes;
    }

    public function getHotRes($cateid)
    {
        $cateM = new Cate();
        $allCateId = $cateM->getchildrenid($cateid);
        $artRes = db('article')->where("cateid IN($allCateId)")->order('click desc')->limit(5)->select();
        return $artRes;
    }

    // 搜索页面的左侧边栏
    public function searchHot()
    {
        $searHot = db('article')
            ->order('click DESC')
            ->limit(5)
            ->select();
        return $searHot;
    }

    //首页侧边栏
    public function getSiteHot()
    {
        $siteHotArt = $this->field('id,title,thumb')->order('click')->limit(5)->select();
        return $siteHotArt;
    }

    public function getNewArticle()
    {
        $newArticleRes = db('article')
            ->field('a.id,a.title,a.des,a.thumb,a.click,a.zan,a.time,c.catename')->alias('a')
            ->join('bk_cate c','a.cateid=c.id')
            ->order('a.id DESC')
            ->limit(10)
            ->select();
        return $newArticleRes;
    }

    //是否推荐, 首页轮播图
    public function getRecArt(){
        $recArt =  $this->where('rec','=',1)
            ->field('id,title,thumb')
            ->order('id DESC')
            ->limit(4)
            ->select();
        return $recArt;
    }


}
